# Movie App - Application permettant de lister les films de différents types y compris les films populaires et les pièces de théâtres

# Démarrage

## Ce qu'il faut

Il faut que **Ionic 3, Angular 5, Cordova et Node.js** soient installés sur votre ordinateur

## Installation

Récupérer le projet : `git clone git@gitlab.com:jugur/jugurthadjennaoui_m2fic_vti_ionic.git`.

Installer les modules : `npm install`

## Lancement de l'application

Tester l'application : `ionic serve --lab`

# Fonctionnement de l'application

<img src="screenshots/Capture1.PNG" width="300px"/>

<img src="screenshots/Capture2.PNG" width="300px"/>

<img src="screenshots/Capture3.png" width="300px"/>

Remarque : L'application ne fonctionne partiellement dû sans doute aux problèmes de performance dans l'API.

# Informations supplémentaires

## Source de la BDD

[lien](https://www.themoviedb.org/documentation/api)

## Auteur

Jugurtha Djennaoui
